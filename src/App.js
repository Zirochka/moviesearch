import { createBrowserRouter, RouterProvider} from "react-router-dom";
import { Home } from "./pages/Home/Home";
import { Movie } from "./pages/Movie/Movie";

import './App.sass';

function App() {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Home />,
    },
    {
      path: "movie/:imdbID",
      element: <Movie />
    }
  ]);

  return (
    <div className="App">
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
