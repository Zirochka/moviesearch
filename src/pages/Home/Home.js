import React, { useCallback, useState } from "react";
import { Search } from "../../components/Search/Search";
import { Card } from "../../components/Card/Card";

export const Home = () => {
  const [movie, setMovie] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const handleSearch = useCallback((search) => {
    setIsLoading(true);
    const url =
      `http://www.omdbapi.com/?apikey=74ed9dbe&t=${search?.title}&y=${search?.year}`;

    const fetchData = async () => {
      try {
        const res = await fetch(url)
        if (!res.ok) {
          throw new Error(res.statusText)
        }
        const data = await res.json()

        setMovie(data);
        setIsLoading(false);
      } catch (err) {
        if (err.name === "AbortError") {
          console.log("the fetch was aborted")
        } else {
          setIsLoading(false);
          console.log('Could not fetch the data')
        }
      }
    }

    fetchData();
  }, []);

  return (
    <div className="home">
      <h1 className="home__title">Find movie</h1>

      <Search onSearch={handleSearch} />

      {isLoading && <p className="home__text">Loading...</p>}

      {movie && <Card movie={movie} />}

      {!movie && !isLoading && <p className="home__text">No movies...</p>}
    </div>
  )
}
