import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { MovieInfo } from "../../components/MovieInfo/MovieInfo";

export const Movie = () => {
  const { imdbID } = useParams();
  const [movie, setMovie] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const url =
      `http://www.omdbapi.com/?apikey=74ed9dbe&i=${imdbID}`;

    const fetchData = async () => {
      try {
        const res = await fetch(url);
        if (!res.ok) {
          throw new Error(res.statusText);
        }
        const data = await res.json();

        setMovie(data);
        setIsLoading(false);
      } catch (err) {}
    }

    fetchData();
  }, [imdbID]);

  return (
    <div className="movie">
      <h1 className="movie__title">Movie</h1>
      {isLoading && <p className="movie__text">Loading...</p>}
      {movie && <MovieInfo movie={movie}/>}
    </div>
  );
}
