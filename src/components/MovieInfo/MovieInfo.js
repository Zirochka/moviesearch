import React from "react";

export const MovieInfo = ({ movie }) => {
  const { Title, Poster, Plot, Genre, Director, Released, imdbRating, Country, Error } = movie;

  return Error ? (
    <p>{Error}</p>
  ) : (
    <div className="movie-info">
      {Poster !== 'N/A' && <img alt={Title} src={Poster} className="movie-info__img" />}
      <h2 className="movie-info__title">{Title}</h2>
      <p className="movie-info__desc">{Plot}</p>

      <ul className="movie-info__list">
        <li className="movie-info__item">
          <span>imdbRating:</span> {imdbRating}
        </li>
        <li className="movie-info__item">
          <span>Genre:</span> {Genre}
        </li>
        <li className="movie-info__item">
          <span>Director:</span> {Director}
        </li>
        <li className="movie-info__item">
          <span>Released:</span> {Released}
        </li>
        <li className="movie-info__item">
          <span>Country:</span> {Country}
        </li>
      </ul>
    </div>
  )
}
