import React from "react";
import { Link } from "react-router-dom";

export const Card = ({ movie }) => {
  const { Title, Poster, Plot, imdbID, Error } = movie;

  return Error ? (
    <p>{Error}</p>
  ) : (
    <div className="card">
      {Poster !== 'N/A' && <img alt={Title} src={Poster} className="card__img" />}
      <div className="card__wrap">
        <h2 className="card__title">{Title}</h2>
        <p className="card__desc">{Plot}</p>
        <Link className="card__link" to={`/movie/${imdbID}`}>More info</Link>
      </div>
    </div>
  )
}
