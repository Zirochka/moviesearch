import React, { useState } from "react";

export const Search = ({ onSearch }) => {
  const [data, setData] = useState(null);

  const handleChange = (event) => {
    setData(prevState => ({...prevState, [event.target.name]: event.target.value}))
  }

  const handleSearch = (event) => {
    event.preventDefault();
    if (data.title || data.year) onSearch(data);
  }

  return (
    <form className="search" onSubmit={handleSearch}>
      <div className={`search__input ${data?.title ? 'is-filled' : ''}`}>
        <label>Title</label>
        <input name="title" value={data?.title} onChange={handleChange} />
      </div>

      <div className={`search__input ${data?.year ? 'is-filled' : ''}`}>
        <label>Year</label>
        <input name="year" value={data?.year} onChange={handleChange} />
      </div>

      <button className="search__button">Search</button>
    </form>
  )
}
